#!/bin/env python3

if __name__ == "__main__":
    with open("input.txt") as in_file:
        current_elf_num = 1
        current_elf_total = 0
        largest_elf_num = 0
        largest_elf_total = 0

        for line in in_file.readlines():
            if line.strip() == "":
                if current_elf_total > largest_elf_total:
                    print(
                        f"Elf #{current_elf_num} has more calories ({current_elf_total}) "
                        f"than the previous elf (#{largest_elf_num}, {largest_elf_total})"
                    )
                    largest_elf_num = current_elf_num
                    largest_elf_total = current_elf_total
                current_elf_num += 1
                current_elf_total = 0

            else:
                current_elf_total += int(line.strip())

        print(f"There are {current_elf_num} elves")
        print(f"Elf #{largest_elf_num} has the most calories: {largest_elf_total}")
