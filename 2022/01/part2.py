#!/bin/env python3

if __name__ == "__main__":
     with open("input.txt") as in_file:
        elf_totals = []

        current_elf_num = 1
        current_elf_total = 0

        for line in in_file.readlines():
            if line.strip() == "":
                elf_totals.append((current_elf_total, current_elf_num))

                current_elf_num += 1
                current_elf_total = 0

            else:
                current_elf_total += int(line.strip())

        sorted_elf_totals = sorted(elf_totals)

        top_three = sorted_elf_totals[-3:]
        print(top_three)

        top_three_sum = sum([calories for (calories, _) in top_three])
        print(f"The top 3 elves are carrying {top_three_sum} calories")
