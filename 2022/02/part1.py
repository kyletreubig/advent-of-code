#!/bin/env python3

Opponent_Choices = {'A': 'rock', 'B': 'paper', 'C': 'scissors'}
My_Choices = {'X': 'rock', 'Y': 'paper', 'Z': 'scissors'}

Choice_Scores = {'rock': 1, 'paper': 2, 'scissors': 3}
Outcome_Scores = {'draw': 3, 'win': 6, 'lose': 0}

Defeats = {'rock': 'scissors', 'scissors': 'paper', 'paper': 'rock'}


def get_outcome(my_choice, their_choice):
    if my_choice == their_choice:
        return 'draw'
    if Defeats[my_choice] == their_choice:
        return 'win'
    if Defeats[their_choice] == my_choice:
        return 'lose'
    raise Exception('Impossible outcome')


if __name__ == "__main__":
    with open("input.txt") as infile:
        round_num = 1
        total_score = 0

        for line in infile.readlines():
            choices = line.strip().split(' ')
            their_choice = Opponent_Choices[choices[0]]
            my_choice = My_Choices[choices[1]]

            outcome = get_outcome(my_choice, their_choice)
            round_score = Choice_Scores[my_choice] + Outcome_Scores[outcome]
            print(f"Round #{round_num}: {my_choice} vs {their_choice}: {outcome} ({round_score})")

            round_num += 1
            total_score += round_score

        print(f"After {round_num - 1} rounds, my total score is {total_score}")

