#!/bin/env python3

Opponent_Choices = {'A': 'rock', 'B': 'paper', 'C': 'scissors'}
Desired_Outcomes = {'X': 'lose', 'Y': 'draw', 'Z': 'win'}

Choice_Scores = {'rock': 1, 'paper': 2, 'scissors': 3}
Outcome_Scores = {'draw': 3, 'win': 6, 'lose': 0}

Defeats = {'rock': 'scissors', 'scissors': 'paper', 'paper': 'rock'}

def get_choice_for_outcome(their_choice, desired_outcome):
    if desired_outcome == 'draw':
        return their_choice
    if desired_outcome == 'lose':
        return Defeats[their_choice]
    if desired_outcome == 'win':
        return [c for c,d in Defeats.items() if d == their_choice][0]
    raise Exception('Impossible outcome')


if __name__ == "__main__":
    with open("input.txt") as infile:
        round_num = 1
        total_score = 0

        for line in infile.readlines():
            choices = line.strip().split(' ')
            their_choice = Opponent_Choices[choices[0]]
            outcome = Desired_Outcomes[choices[1]]

            my_choice = get_choice_for_outcome(their_choice, outcome)
            round_score = Choice_Scores[my_choice] + Outcome_Scores[outcome]
            print(f"Round #{round_num}: {my_choice} vs {their_choice}: {outcome} ({round_score})")

            round_num += 1
            total_score += round_score

        print(f"After {round_num - 1} rounds, my total score is {total_score}")

