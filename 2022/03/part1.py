#!/usr/bin/env python3

Priorities = ' abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

if __name__ == "__main__":
    with open("input.txt") as infile:
        sum_priorities = 0
        for line in infile.readlines():
            line = line.strip()
            total_item_count = len(line)
            compartment_count = int(total_item_count / 2)
            compartment_1_items = line[:compartment_count]
            compartment_2_items = line[compartment_count:]
            if len(compartment_1_items) != len(compartment_2_items):
                raise Exception('Unequal number of items in each component')
            comp_1_item_set = set(compartment_1_items)
            comp_2_item_set = set(compartment_2_items)
            common_items = comp_1_item_set.intersection(comp_2_item_set)
            for common_item in common_items:
                priority = Priorities.index(common_item)
                print(f"{common_item} = {priority}")
                sum_priorities += priority

        print(f"Total of priorities is {sum_priorities}")
