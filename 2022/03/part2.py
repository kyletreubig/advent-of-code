#!/usr/bin/env python3

Priorities = ' abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

if __name__ == "__main__":
    with open("input.txt") as infile:
        sum_priorities = 0

        group = []

        for line in infile.readlines():
            line = line.strip()
            group.append(set(line))

            if len(group) == 3:
                common_items = group[0].intersection(group[1]).intersection(group[2])
                if len(common_items) != 1:
                    raise Exception('Group has more than one common item')
                for common_item in common_items:
                    priority = Priorities.index(common_item)
                    print(f"Group has badge {common_item} ({priority})")
                    sum_priorities += priority

                group = []

        print(f"Total of priorities is {sum_priorities}")
