#!/usr/bin/env python3

def contains(lhs, rhs):
    return int(lhs[0]) <= int(rhs[0]) and int(lhs[1]) >= int(rhs[1])

if __name__ == "__main__":
    with open("input.txt") as infile:
        num_redundant = 0
        for line in infile.readlines():
            parts = line.strip().split(",")
            if len(parts) != 2:
                raise Exception("Invalid line")
            range_1 = parts[0].split("-")
            range_2 = parts[1].split("-")
            if contains(range_1, range_2) or contains(range_2, range_1):
                print(f"Ranges {range_1} and {range_2} completely overlaps")
                num_redundant += 1

        print(f"There are {num_redundant} pairs with complete overlaps")
