#!/usr/bin/env python3

def overlaps(lhs, rhs):
    lhs_lower = int(lhs[0])
    lhs_upper = int(lhs[1])
    rhs_lower = int(rhs[0])
    rhs_upper = int(rhs[1])
    return lhs_lower <= rhs_lower and lhs_upper >= rhs_lower

if __name__ == "__main__":
    with open("input.txt") as infile:
        num_redundant = 0
        for line in infile.readlines():
            parts = line.strip().split(",")
            if len(parts) != 2:
                raise Exception("Invalid line")
            range_1 = parts[0].split("-")
            range_2 = parts[1].split("-")
            if overlaps(range_1, range_2) or overlaps(range_2, range_1):
                print(f"Ranges {range_1} and {range_2} partially overlaps")
                num_redundant += 1

        print(f"There are {num_redundant} pairs with overlaps")
