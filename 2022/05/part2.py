#!/usr/bin/env python3
from collections import deque
from typing import List

num_crates = 9

def print_stacks(stacks):
    for stack in stacks:
        print(stack)

if __name__ == "__main__":
    stacks: List[deque[str]] = []
    for _ in range(num_crates):
        stacks.append(deque())

    with open("input.txt") as infile:
        for line in infile.readlines():
            if "[" in line:
                for idx in range(num_crates):
                    start = idx * 4
                    crate = line[start:start+3].strip()
                    if len(crate) > 1:
                        stacks[idx].append(crate[1])

            if "move" in line:
                parts = line.split(" ")
                quantity = int(parts[1])
                source = int(parts[3])
                dest = int(parts[5])
                
                source_stack = stacks[source - 1]
                dest_stack = stacks[dest - 1]

                to_move = []
                for _ in range(quantity):
                    to_move.append(source_stack.popleft())
                for crate in reversed(to_move):
                    dest_stack.appendleft(crate)

    print_stacks(stacks)

    message = ""
    for stack in stacks:
        message += stack[0]
    print(f"Final message is {message}")