#!//usr/bin/env python3

from collections import deque

if __name__ == "__main__":
    with open("input.txt") as infile:
        instream = infile.read().strip()

        pos = 1
        last_4_chars = deque()
        for char in instream:
            last_4_chars.append(char)
            if len(last_4_chars) == 5:
                last_4_chars.popleft()
            if len(set(last_4_chars)) == 4:
                print(f"Found 4 unique characters at position {pos} ({last_4_chars})")
                break
            pos += 1
