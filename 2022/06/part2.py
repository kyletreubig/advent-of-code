#!//usr/bin/env python3

from collections import deque

if __name__ == "__main__":
    with open("input.txt") as infile:
        instream = infile.read().strip()

        pos = 1
        last_chars = deque()
        for char in instream:
            last_chars.append(char)
            if len(last_chars) == 15:
                last_chars.popleft()
            if len(set(last_chars)) == 14:
                print(f"Found 14 unique characters at position {pos} ({last_chars})")
                break
            pos += 1
