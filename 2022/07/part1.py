#!/usr/bin/env python3

from typing import List, Optional

class File:

    def __init__(self, name: str, size: int):
        self.name = name
        self.size = size

    def print(self, indent: int = 0):
        print(f"{'  ' * indent}- {self.name} (file, size={self.size})")


class Directory:

    def __init__(self, name: str, parent: Optional['Directory'] = None):
        self.name = name
        self.parent = parent
        self.directories: List[Directory] = []
        self.files: List[File] = []

    def print(self, indent: int = 0):
        print(f"{'  ' * indent}- {self.name} (dir)")
        for child_dir in self.directories:
            child_dir.print(indent + 1)
        for child_file in self.files:
            child_file.print(indent + 1)

    def size(self):
        my_size = 0
        for child_dir in self.directories:
            my_size += child_dir.size()
        for child_file in self.files:
            my_size += child_file.size
        return my_size

    def abs_path(self):
        if self.parent:
            return f"{self.parent.abs_path()}/{self.name}".replace("//", "/")
        return self.name

    def visit(self, func):
        func(self)
        for child_dir in self.directories:
            child_dir.visit(func)


if __name__ == "__main__":
    with open("input.txt") as infile:
        root_dir = Directory("/")

        current_dir = root_dir
        for line in infile.readlines():
            if line.startswith("$ cd "):
                cd_dest = line[5:].strip()
                if cd_dest == "/":
                    current_dir = root_dir
                elif cd_dest == "..":
                    if not current_dir.parent:
                        raise Exception(f"Current dir {current_dir.name} has no parent dir")
                    current_dir = current_dir.parent
                else:
                    current_dir = next(d for d in current_dir.directories if d.name == cd_dest)
            elif line.strip() == "$ ls":
                if current_dir.directories or current_dir.files:
                    raise Exception(f"ls ran a second time for directory {current_dir.name}")
            elif line.startswith("dir "):
                dir_name = line[4:].strip()
                current_dir.directories.append(Directory(dir_name, current_dir))
            else: # assume file
                parts = line.strip().split(" ")
                current_dir.files.append(File(parts[1], int(parts[0])))

        root_dir.print()

        dirs_under_100000: List[Directory] = []
        def add_if_under_100000(directory: Directory):
            if directory.size() < 100000:
                dirs_under_100000.append(directory)
        root_dir.visit(add_if_under_100000)

        total_size = 0
        for directory in dirs_under_100000:
            total_size += directory.size()

        print(f"Total size of all dirs under 100000 is {total_size}")
