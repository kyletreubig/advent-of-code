#!/usr/bin/env python3

from typing import List

if __name__ == "__main__":
    rows: List[List[int]] = []
    with open("input.txt") as infile:
        for line in infile.readlines():
            rows.append([int(x) for x in line.strip()])

    num_rows = len(rows)
    num_cols = len(rows[0])
    print(f"There are {num_rows}x{num_cols} trees")

    def is_visible(row: int, col: int):
        my_height = rows[row][col]
        # print(f"Tree {row}x{col} is {my_height} tall")
        visible = 0b1111 # Start with assumed visibility from all sides
        for left in range(0, col):
            if rows[row][left] >= my_height:
                # print(f"    it is not visible from left because of tree {row}x{left}")
                visible &= 0b0111 # Not visible from left
                break
        for right in range(col + 1, num_cols):
            if rows[row][right] >= my_height:
                # print(f"    it is not visible from right because of tree {row}x{right}")
                visible &= 0b1011 # Not visible from right
                break
        for above in range(0, row):
            if rows[above][col] >= my_height:
                # print(f"    it is not visible from above because of tree {above}x{col}")
                visible &= 0b1101 # Not visible from above
                break
        for below in range(row + 1, num_rows):
            if rows[below][col] >= my_height:
                # print(f"    it is not visible from below because of tree {below}x{col}")
                visible &= 0b1110 # Not visible from below
                break
        # print(f"  tree visibility is {bin(visible)}")
        return visible != 0

    num_visible = 0
    for row in range(num_rows):
        for col in range(num_cols):
            if is_visible(row, col):
                # print(f"  Tree {row}x{col} is visible")
                num_visible += 1

    print(f"There are {num_visible} trees that are visible")
