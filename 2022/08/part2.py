#!/usr/bin/env python3

from typing import List

if __name__ == "__main__":
    rows: List[List[int]] = []
    with open("input.txt") as infile:
        for line in infile.readlines():
            rows.append([int(x) for x in line.strip()])

    num_rows = len(rows)
    num_cols = len(rows[0])
    print(f"There are {num_rows}x{num_cols} trees")

    def get_scenic_score(row: int, col: int):
        my_height = rows[row][col]
        # print(f"height of {row}x{col} is {my_height}")
        score = 0
        for left in range(col - 1, -1, -1):
            score += 1
            if rows[row][left] >= my_height:
                break
        # print(f"  score left is {score}")
        total_score = score
        score = 0
        for right in range(col + 1, num_cols):
            score += 1
            if rows[row][right] >= my_height:
                break
        total_score *= score
        # print(f"  score right is {score}, total score is {total_score}")
        score = 0
        for above in range(row - 1, -1, -1):
            # print(f"    checking {above}x{col}")
            score += 1
            if rows[above][col] >= my_height:
                # print(f"    tree at {above}x{col} is higher, score is {score}")
                break
        total_score *= score
        # print(f"  score above is {score}, total score is {total_score}")
        score = 0
        for below in range(row + 1, num_rows):
            score += 1
            if rows[below][col] >= my_height:
                break
        total_score *= score
        # print(f"  score below is {score}, total score is {total_score}")
        return total_score

    best_score = 0
    for row in range(num_rows):
        for col in range(num_cols):
            score = get_scenic_score(row, col)
            if score > best_score:
                best_score = score

    print(f"The best scenic score is {best_score}")
