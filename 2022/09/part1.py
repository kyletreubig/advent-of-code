#!/usr/bin/env python3

def move(pos, direction):
    if direction == "U":
        return (pos[0], pos[1] + 1)
    if direction == "D":
        return (pos[0], pos[1] - 1)
    if direction == "L":
        return (pos[0] - 1, pos[1])
    if direction == "R":
        return (pos[0] + 1, pos[1])
    raise Exception(f"Invalid direction: {direction}")


def is_adjacent(head_pos, tail_pos):
    if abs(head_pos[0] - tail_pos[0]) > 1:
        return False
    if abs(head_pos[1] - tail_pos[1]) > 1:
        return False
    return True


def move_to_adjacent(head_pos, tail_pos):
    if head_pos[0] == tail_pos[0]:  # Same column
        if head_pos[1] == tail_pos[1] - 2:
            return (tail_pos[0], tail_pos[1] - 1)
        if head_pos[1] == tail_pos[1] + 2:
            return (tail_pos[0], tail_pos[1] + 1)
        raise Exception("Head and tail are in same column, but are not 2 rows apart")
    if head_pos[1] == tail_pos[1]:  # Same row
        if head_pos[0] == tail_pos[0] - 2:
            return (tail_pos[0] - 1, tail_pos[1])
        if head_pos[0] == tail_pos[0] + 2:
            return (tail_pos[0] + 1, tail_pos[1])
        raise Exception("Head and tail are in same row, but are not 2 columns apart")
    # Have to move diagonally
    if head_pos[0] < tail_pos[0]:  # Head is left of tail
        if head_pos[1] < tail_pos[1]:  # Head is below tail
            return (tail_pos[0] - 1, tail_pos[1] - 1)  # Move left and down
        return (tail_pos[0] - 1, tail_pos[1] + 1)  # Move left and up
    else:  # Head is right of tail
        if head_pos[1] < tail_pos[1]:  # Head is below tail
            return (tail_pos[0] + 1, tail_pos[1] - 1)  # Move right and down
        return (tail_pos[0] + 1, tail_pos[1] + 1)   # Move right and up


if __name__ == "__main__":
    with open("input.txt") as infile:
        visited = set()

        head_pos = (0, 0)
        tail_pos = (0, 0)
        visited.add(tail_pos)

        for line in infile.readlines():
            parts = line.split(" ")
            direction = parts[0].strip()
            distance = int(parts[1].strip())

            for _ in range(distance):
                head_pos = move(head_pos, direction)
                if not is_adjacent(head_pos, tail_pos):
                    tail_pos = move_to_adjacent(head_pos, tail_pos)
                    visited.add(tail_pos)

    print(f"The tail visited {len(visited)} positions")
