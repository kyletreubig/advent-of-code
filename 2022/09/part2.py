#!/usr/bin/env python3

def move(pos, direction):
    if direction == "U":
        return (pos[0], pos[1] + 1)
    if direction == "D":
        return (pos[0], pos[1] - 1)
    if direction == "L":
        return (pos[0] - 1, pos[1])
    if direction == "R":
        return (pos[0] + 1, pos[1])
    raise Exception(f"Invalid direction: {direction}")


def is_adjacent(head_pos, tail_pos):
    if abs(head_pos[0] - tail_pos[0]) > 1:
        return False
    if abs(head_pos[1] - tail_pos[1]) > 1:
        return False
    return True


def move_to_adjacent(head_pos, tail_pos):
    if head_pos[0] == tail_pos[0]:  # Same column
        if head_pos[1] == tail_pos[1] - 2:
            return (tail_pos[0], tail_pos[1] - 1)
        if head_pos[1] == tail_pos[1] + 2:
            return (tail_pos[0], tail_pos[1] + 1)
        raise Exception("Head and tail are in same column, but are not 2 rows apart")
    if head_pos[1] == tail_pos[1]:  # Same row
        if head_pos[0] == tail_pos[0] - 2:
            return (tail_pos[0] - 1, tail_pos[1])
        if head_pos[0] == tail_pos[0] + 2:
            return (tail_pos[0] + 1, tail_pos[1])
        raise Exception("Head and tail are in same row, but are not 2 columns apart")
    # Have to move diagonally
    if head_pos[0] < tail_pos[0]:  # Head is left of tail
        if head_pos[1] < tail_pos[1]:  # Head is below tail
            return (tail_pos[0] - 1, tail_pos[1] - 1)  # Move left and down
        return (tail_pos[0] - 1, tail_pos[1] + 1)  # Move left and up
    else:  # Head is right of tail
        if head_pos[1] < tail_pos[1]:  # Head is below tail
            return (tail_pos[0] + 1, tail_pos[1] - 1)  # Move right and down
        return (tail_pos[0] + 1, tail_pos[1] + 1)   # Move right and up


class Visual:

    def __init__(self):
        self.x = (0, 0)
        self.y = (0, 0)

    def adjust_bounds(self, new_pos):
        self.x = (min(self.x[0], new_pos[0]), max(self.x[1], new_pos[0]))
        self.y = (min(self.y[0], new_pos[1]), max(self.y[1], new_pos[1]))

    def print_knots(self, knots, header = ""):
        chars = {}
        for idx, knot in reversed(list(enumerate(knots))):
            chars[knot] = idx if idx > 0 else "H"

        print(f"\n== {header}")

        for y in range(self.y[1], self.y[0] -1, -1):
            for x in range(self.x[0], self.x[1] + 1):
                if (x, y) in chars:
                    print(chars[(x, y)], end="")
                else:
                    print(".", end="")
            print("")


if __name__ == "__main__":
    with open("input.txt") as infile:
        visited = set()

        knots = []
        for _ in range(10):
            knots.append((0, 0))
        visited.add(knots[-1])

        visual = Visual()
        visual.print_knots(knots)
        
        round = 0
        for line in infile.readlines():
            parts = line.split(" ")
            direction = parts[0].strip()
            distance = int(parts[1].strip())

            for _ in range(distance):
                prev_knot = None
                for (idx, knot) in enumerate(knots):
                    if idx == 0:  # Move the head according to the input
                        knots[0] = move(knot, direction)
                        prev_knot = knots[0]
                    elif prev_knot:
                        if not is_adjacent(prev_knot, knot):
                            knot = move_to_adjacent(prev_knot, knot)
                            knots[idx] = knot
                            if idx == 9:
                                visited.add(knot)
                        prev_knot = knot
                    visual.adjust_bounds(prev_knot)

            # visual.print_knots(knots, line)
            # round += 1
            # if round == 10:
            #     break

    print(f"The tail visited {len(visited)} positions")
