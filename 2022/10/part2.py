#!/usr/bin/env python3

from collections import deque

if __name__ == "__main__":
    with open("input.txt") as infile:

        instructions = deque()

        for line in infile.readlines():
            parts = line.split(" ")
            if parts[0].strip() == "noop":
                instructions.append((1, 0))
            elif parts[0] == "addx":
                instructions.append((2, int(parts[1])))
            else:
                raise Exception(f"Unrecognized instruction: {parts}")

        x_register = 1
        cycle_num = 1

        while cycle_num <= 240:
            pixel = "."

            x_pos = (cycle_num - 1) % 40
            if x_pos >= x_register - 1 and x_pos <= x_register + 1:
                pixel = "#"

            print(pixel, end="")
            if cycle_num % 40 == 0:
                print("")

            current = instructions[0]
            current = (current[0] - 1, current[1])
            # print(f"Cycle {cycle_num}, operation needs {current[0]} more cycle(s)")
            if current[0] == 0:
                # print(f"adding {current[1]} to X register")
                x_register += current[1]
                instructions.popleft()
            else:
                instructions[0] = current

            cycle_num += 1
            # if cycle_num > 10:
            #     break
