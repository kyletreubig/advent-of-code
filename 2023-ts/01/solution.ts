import * as fs from 'fs';

function part1(filename: string): number {
    const contents = fs.readFileSync(filename);
    let sum = 0;
    for (const line of contents.toString().split('\n')) {
        const chars = line.split('').filter(c => c >= '0' && c <= '9');
        const value = parseInt(`${chars[0]}${chars[chars.length - 1]}`);
        // console.log("value for line", line, "is", value)
        sum += value
    }
    return sum;
}

console.log("part1 solution for example:", part1("example.txt"));
console.log("part1 solution for input:", part1("input.txt"));

const digitStrings = {
    zero: 0,
    one: 1,
    two: 2,
    three: 3,
    four: 4,
    five: 5,
    six: 6,
    seven: 7,
    eight: 8,
    nine: 9,
};

function part2(filename: string): number {
    const contents = fs.readFileSync(filename);
    let sum = 0;
    for (const line of contents.toString().split('\n')) {
        const digits: number[] = [];
        let buf: string[] = [];
        for (const c of line.split('')) {
            if (c >= '0' && c <= '9') {
                digits.push(parseInt(c));
                buf = [];
            } else {
                buf.push(c);
                for (const [digitStr, digitVal] of Object.entries(digitStrings)) {
                    // console.log("does", buf.join(''), "contain", digitStr);
                    if (buf.join('').includes(digitStr)) {
                        digits.push(digitVal);
                        buf = buf.slice(buf.length - 1); // leave the last char for overlapping
                    }
                }
            }
        }
        // console.log("digits for line", line, "is", digits);
        const value = parseInt(`${digits[0]}${digits[digits.length - 1]}`);
        // console.log("value for line", line, "is", value);
        sum += value;
    }
    return sum;
}

console.log("part2 solution for example:", part2("example2.txt"));
console.log("part2 solution for input:", part2("input.txt"));
