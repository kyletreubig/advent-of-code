import * as fs from 'fs';

function part1(filename: string): number {
    const maxCounts: Record<string, number> = {
        red: 12,
        green: 13,
        blue: 14,
    };
    let possibleGameNumSum = 0;

    const contents = fs.readFileSync(filename);
    for (const line of contents.toString().split('\n')) {
        const gameNum = parseInt(line.slice(5, line.indexOf(':')));
        const subsets = line.slice(line.indexOf(':') + 1);
        let possible = true;
        for (const subset of subsets.split(';')) {
            for (const count_color of subset.split(',')) {
                let [count, color] = count_color.trim().split(' ');
                color = color.trim();
                if (color in maxCounts) {
                    const countVal = parseInt(count.trim());
                    if (countVal > maxCounts[color]) {
                        possible = false;
                    }
                }
            }
        }
        if (possible) {
            possibleGameNumSum += gameNum;
        }
    }

    return possibleGameNumSum;
}

console.log("part1 solution for example:", part1("example1.txt"));
console.log("part1 solution for input:", part1("input.txt"));

function part2(filename: string): number {
    let totalPower = 0;

    const contents = fs.readFileSync(filename);
    for (const line of contents.toString().split('\n')) {
        const minCounts: Record<string, number> = {
            red: 0,
            green: 0,
            blue: 0,
        };

        const gameNum = parseInt(line.slice(5, line.indexOf(':')));
        const subsets = line.slice(line.indexOf(':') + 1);
        let possible = true;
        for (const subset of subsets.split(';')) {
            for (const count_color of subset.split(',')) {
                let [count, color] = count_color.trim().split(' ');
                color = color.trim();
                if (color in minCounts) {
                    const countVal = parseInt(count.trim());
                    minCounts[color] = Math.max(minCounts[color], countVal);
                }
            }
        }
        const gamePower = minCounts.red * minCounts.green * minCounts.blue;
        totalPower += gamePower;
    }

    return totalPower;
}

console.log("part2 solution for example:", part2("example1.txt"));
console.log("part2 solution for input:", part2("input.txt"));