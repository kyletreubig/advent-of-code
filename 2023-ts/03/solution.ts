import * as fs from 'fs';

type GridValue = {
    row: number;
    col: number;
    value: number;
    length: number;
};
type GridSymbol = {
    row: number;
    col: number;
    isGear: boolean;
};

function isAdjacentToSymbol(value: GridValue, symbols: GridSymbol[]) {
    for (const symbol of symbols) {
        // If within reachable column
        if (symbol.col >= value.col - 1 && symbol.col <= value.col + value.length) {
            // If within reachable row
            if (symbol.row >= value.row - 1 && symbol.row <= value.row + 1) {
                return true;
            }
        }
    }
    return false;
}

function part1(filename: string) {
    const contents = fs.readFileSync(filename);
    const grid: string[][] = [];
    for (const line of contents.toString().split("\n")) {
        grid.push(line.split(""));
    }

    const values: GridValue[] = [];
    const symbols: GridSymbol[] = [];

    for (let row = 0; row < grid.length; ++row) {
        let buf: string[] = [];
        for (let col = 0; col < grid[row].length; ++col) {
            const c = grid[row][col];
            if (c >= '0' && c <= '9') {
                buf.push(c);
            } else {
                if (buf.length > 0) {
                    values.push({
                        row,
                        col: col - buf.length,
                        value: parseInt(buf.join("")),
                        length: buf.length,
                    });
                    buf = [];
                }
                if (c != '.') {
                    symbols.push({ row, col, isGear: false });
                }
            }
        }
        if (buf.length > 0) {
            values.push({
                row,
                col: grid[row].length - buf.length,
                value: parseInt(buf.join("")),
                length: buf.length,
            });
        }
    }

    let sumPartNums = 0;
    for (const value of values) {
        if (isAdjacentToSymbol(value, symbols)) {
            sumPartNums += value.value;
        }
    }

    return sumPartNums;
}

console.log("part1 solution for example", part1("example1.txt"));
console.log("part1 solution for input", part1("input.txt"));

function findAdjacentParts(symbol: GridSymbol, values: GridValue[]) {
    const parts: GridValue[] = [];
    for (const value of values) {
        // If within reachable column
        if (symbol.col >= value.col - 1 && symbol.col <= value.col + value.length) {
            // If within reachable row
            if (symbol.row >= value.row - 1 && symbol.row <= value.row + 1) {
                parts.push(value);
            }
        }
    }
    return parts;
}

function part2(filename: string) {
    const contents = fs.readFileSync(filename);
    const grid: string[][] = [];
    for (const line of contents.toString().split("\n")) {
        grid.push(line.split(""));
    }

    const values: GridValue[] = [];
    const symbols: GridSymbol[] = [];

    for (let row = 0; row < grid.length; ++row) {
        let buf: string[] = [];
        for (let col = 0; col < grid[row].length; ++col) {
            const c = grid[row][col];
            if (c >= '0' && c <= '9') {
                buf.push(c);
            } else {
                if (buf.length > 0) {
                    values.push({
                        row,
                        col: col - buf.length,
                        value: parseInt(buf.join("")),
                        length: buf.length,
                    });
                    buf = [];
                }
                if (c != '.') {
                    symbols.push({ row, col, isGear: c == '*' });
                }
            }
        }
        if (buf.length > 0) {
            values.push({
                row,
                col: grid[row].length - buf.length,
                value: parseInt(buf.join("")),
                length: buf.length,
            });
        }
    }

    let sumGearRatios = 0;
    for (const symbol of symbols) {
        const adjParts = findAdjacentParts(symbol, values);
        if (adjParts.length == 2) {
            const gearRatio = adjParts[0].value * adjParts[1].value;
            sumGearRatios += gearRatio;
        }
    }

    return sumGearRatios;
}

console.log("part2 solution for example", part2("example1.txt"));
console.log("part2 solution for input", part2("input.txt"));