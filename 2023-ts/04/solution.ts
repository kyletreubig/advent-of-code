import * as fs from 'fs';

function part1(filename: string) {
    const contents = fs.readFileSync(filename);
    let sumPoints = 0;
    for (const line of contents.toString().split('\n')) {
        const card = line.split(':')[1].split('|');
        const winningNums = card[0].trim().split(' ').filter(num => num !== '');
        const cardNums = card[1].trim().split(' ').filter(num => num !== '');
        const count = cardNums.filter(num => winningNums.includes(num)).length;
        if (count > 0) {
            const points = Math.pow(2, count - 1);
            sumPoints += points;
        }
    }
    return sumPoints;
}

console.log("part1 solution for example:", part1("example1.txt"));
console.log("part1 solution for input:", part1("input.txt"));

function part2(filename: string) {
    const contents = fs.readFileSync(filename);
    const cardCopies: Record<number, number> = {};
    let totalNumCards = 0;
    for (const line of contents.toString().split('\n')) {
        const cardNum = parseInt(line.split(':')[0].trim().split(' ').filter(n => n !== '')[1]);
        const card = line.split(':')[1].split('|');
        const winningNums = card[0].trim().split(' ').filter(num => num !== '');
        const cardNums = card[1].trim().split(' ').filter(num => num !== '');
        const count = cardNums.filter(num => winningNums.includes(num)).length;

        const numCopiesOfThisCard = cardNum in cardCopies ? cardCopies[cardNum] : 1;

        for (let i = 0; i < count; ++i) {
            const copiedCardNum = cardNum + i + 1;
            if (copiedCardNum in cardCopies) {
                cardCopies[copiedCardNum] += numCopiesOfThisCard;
            } else {
                cardCopies[copiedCardNum] = 1 + numCopiesOfThisCard;
            }
        }

        totalNumCards += numCopiesOfThisCard;
    }
    return totalNumCards;
}

console.log("part2 solution for example:", part2("example1.txt"));
console.log("part2 solution for input:", part2("input.txt"));