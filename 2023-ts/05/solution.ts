import * as fs from 'fs';

function part1(filename: string) {
    const contents = fs.readFileSync(filename);
    const maps: Record<string, Record<string, Record<number, number>>> = {};
    let seeds: number[] = [];
    let currSrc: string = "";
    let currDest: string = "";
    for (const line of contents.toString().split('\n')) {
        if (line.startsWith("seeds:")) {
            seeds = line.split(":")[1].split(" ").filter(n => n !== '').map(n => parseInt(n));
        } else if (line.endsWith("map:")) {
            [currSrc, currDest] = line.split(" ")[0].split("-to-");
            if (!(currSrc in maps)) {
                maps[currSrc] = {};
            }
            if (!(currDest in maps[currSrc])) {
                maps[currSrc][currDest] = {};
            }
        } else if (line.trim().length > 0) {
            const [destStart, srcStart, len] = line.split(" ").filter(n => n != '').map(n => parseInt(n));
            for (let i = 0; i < len; ++i) {
                maps[currSrc][currDest][srcStart + i] = destStart + i;
            }
        }
    }

    let lowestLocation = Number.MAX_SAFE_INTEGER;
    for (const seed of seeds) {
        let currSrc = "seed";
        let currVal = seed;
        while (currSrc != "location") {
            for (const dest in maps[currSrc]) {
                if (currVal in maps[currSrc][dest]) {
                    // console.log("mapping", currSrc, currVal, "to", dest, maps[currSrc][dest][currVal])
                    currVal = maps[currSrc][dest][currVal];
                } // else currValue remains unchanged
                currSrc = dest;
                break; // only one dest per src
            }
        }
        lowestLocation = Math.min(lowestLocation, currVal);
    }

    return lowestLocation;
}

console.log("part1 solution for example:", part1("example1.txt"))
console.log("part1 solution for input:", part1("input.txt"))