#!/bin/env python3

if __name__ == "__main__":
    with open("input.txt") as infile:
        sum = 0

        for line in infile.readlines():
            line = line.strip()
            digits = [c for c in line if c.isdigit()]
            print(f"Line '{line}' contains digits {digits}")

            value = int(f"{digits[0]}{digits[-1]}")
            print(f"Line value is {value}")
            sum += value

    print(f"Sum is {sum}")