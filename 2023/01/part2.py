#!/bin/env python3

digits = [
    "zero",
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
]

if __name__ == "__main__":
    with open("input.txt") as infile:
        sum = 0

        for line in infile.readlines():
            line = line.strip()

            numbers = []
            buffer = ""
            for c in line:
                if c.isdigit():
                    numbers.append(c)
                    buffer = ""
                else:
                    buffer += c
                    for val, digit in enumerate(digits):
                        if digit in buffer:
                            numbers.append(val)
                            # Keep the last letter because overlapping digits are important
                            buffer = buffer[-1]
                            break

            value = int(f"{numbers[0]}{numbers[-1]}")
            print(f"Line '{line}' contains numbers {numbers} -> {value}")
            sum += value

    print(f"Sum is {sum}")