#!/usr/bin/env python3

if __name__ == "__main__":
    max_counts = dict(
        red=12,
        green=13,
        blue=14,
    )
    possible_game_sum = 0
    impossible_game_sum = 0

    with open("input.txt") as infile:
        for line in infile.readlines():
            game = line[0:line.index(":")]
            game_num = int(game.split(" ")[1])

            possible = True

            cube_subsets = line[line.index(":") + 1:].strip()
            for cube_subset in cube_subsets.split(";"):
                for cube_count in cube_subset.split(","):
                    count, color = cube_count.strip().split(" ")
                    if color not in max_counts:
                        print(f"Found unrecognized color: {color}")
                    if int(count) > max_counts[color]:
                        print(f"Game {game_num} is impossible: {cube_subsets}")
                        possible = False

            if possible:
                possible_game_sum += game_num
            else:
                impossible_game_sum += game_num

    print(f"Sum of possible game numbers: {possible_game_sum}")
    print(f"Sum of impossible game numbers: {impossible_game_sum}")
