#!/usr/bin/env python3

if __name__ == "__main__":
    power_sum = 0
    with open("input.txt") as infile:
        for line in infile.readlines():
            game = line[0:line.index(":")]
            game_num = int(game.split(" ")[1])

            min_counts = dict(
                red=0,
                green=0,
                blue=0,
            )

            cube_subsets = line[line.index(":") + 1:].strip()
            for cube_subset in cube_subsets.split(";"):
                for cube_count in cube_subset.split(","):
                    count, color = cube_count.strip().split(" ")
                    count = int(count)
                    if color not in min_counts:
                        print(f"Found unrecognized color: {color}")
                    if count > min_counts[color]:
                        min_counts[color] = count

            power = min_counts["red"] * min_counts["green"] * min_counts["blue"]
            print(f"Game {game_num} requires {min_counts} and has power {power}")

            power_sum += power

    print(f"Total power is {power_sum}")
