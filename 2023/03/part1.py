#!/usr/bin/env python3


def adjacents(num_coordinate):
    return [
        dict(
            line=line,
            col=col,
        )
        for line in range(
            num_coordinate["line"] - 1,
            num_coordinate["line"] + 2,
        )
        for col in range(
            num_coordinate["col"] - 1,
            num_coordinate["col"] + num_coordinate["length"] + 1,
        )
    ]


if __name__ == "__main__":
    with open("input.txt") as infile:
        numbers = []
        symbols = []

        line_num = 0
        for line in infile.readlines():
            line = line.strip()
            buffer = ""
            col_num = 0
            for c in line:
                if c.isdigit():
                    buffer += c
                else:
                    if buffer:
                        numbers.append(
                            dict(
                                num=int(buffer),
                                length=len(buffer),
                                line=line_num,
                                col=col_num - len(buffer),
                            )
                        )
                        buffer = ""
                    if c != ".":
                        symbols.append(
                            dict(
                                line=line_num,
                                col=col_num,
                            )
                        )
                col_num += 1

            if buffer:
                numbers.append(
                    dict(
                        num=int(buffer),
                        length=len(buffer),
                        line=line_num,
                        col=col_num - len(buffer),
                    )
                )

            line_num += 1

        print(numbers[0:3])
        print(symbols[0:3])
        print(adjacents(numbers[0]))

        sum = 0
        for num in numbers:
            if any([adj in symbols for adj in adjacents(num)]):
                print(f"Part num {num} is adjacent to a symbol")
                sum += num["num"]

        print(f"Total sum of part numbers is {sum}")
