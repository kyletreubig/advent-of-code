#!/usr/bin/env python3


def adjacents(num_coordinate):
    return [
        dict(
            line=line,
            col=col,
        )
        for line in range(
            num_coordinate["line"] - 1,
            num_coordinate["line"] + 2,
        )
        for col in range(
            num_coordinate["col"] - 1,
            num_coordinate["col"] + num_coordinate["length"] + 1,
        )
    ]


if __name__ == "__main__":
    with open("input.txt") as infile:
        numbers = []
        gears = []

        line_num = 0
        for line in infile.readlines():
            line = line.strip()
            buffer = ""
            col_num = 0
            for c in line:
                if c.isdigit():
                    buffer += c
                else:
                    if buffer:
                        numbers.append(
                            dict(
                                num=int(buffer),
                                length=len(buffer),
                                line=line_num,
                                col=col_num - len(buffer),
                            )
                        )
                        buffer = ""
                    if c == "*":
                        gears.append(
                            dict(
                                line=line_num,
                                col=col_num,
                                nums=[],
                            )
                        )
                col_num += 1

            if buffer:
                numbers.append(
                    dict(
                        num=int(buffer),
                        length=len(buffer),
                        line=line_num,
                        col=col_num - len(buffer),
                    )
                )

            line_num += 1

        print(numbers[0:3])
        print(gears[0:3])
        print(adjacents(numbers[3]))

        for num in numbers:
            for adj in adjacents(num):
                for gear in gears:
                    if adj["line"] == gear["line"] and adj["col"] == gear["col"]:
                        gear["nums"].append(num)

        print(gears[0:3])

        sum = 0
        for gear in gears:
            if len(gear["nums"]) == 2:
                ratio = gear["nums"][0]["num"] * gear["nums"][1]["num"]
                sum += ratio

        print(f"Total sum of gear ratios is {sum}")
