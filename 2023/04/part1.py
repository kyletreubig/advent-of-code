#!/usr/bin/env python3
import math

if __name__ == "__main__":
    with open("input.txt") as infile:
        total = 0
        for line in infile.readlines():
            card, nums = line.split(":")
            winning_nums, our_nums = nums.split("|")
            winning_nums = winning_nums.strip().replace("  ", " ").split(" ")
            our_nums = our_nums.strip().replace("  ", " ").split(" ")

            num_winners = sum([n in winning_nums for n in our_nums])
            points = 0 if not num_winners else int(math.pow(2, num_winners - 1))
            print(f"{card} has {num_winners} winning numbers and is worth {points}")
            total += points

        print(f"Total points is {total}")
