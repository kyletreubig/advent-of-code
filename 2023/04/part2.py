#!/usr/bin/env python3

if __name__ == "__main__":
    with open("input.txt") as infile:
        cards = {}
        winners = 0
        for line in infile.readlines():
            card, nums = line.split(":")
            card_num = int(card.split()[-1].strip())
            if str(card_num) not in cards:
                cards[str(card_num)] = 1

            winning_nums, our_nums = nums.split("|")
            winning_nums = winning_nums.strip().split()
            our_nums = our_nums.strip().split()

            num_winners = sum([n in winning_nums for n in our_nums])
            for offset in range(num_winners):
                cards.setdefault(str(card_num + offset + 1), 1)
                cards[str(card_num + offset + 1)] += cards[str(card_num)]
            print(f"{card} has {num_winners} winning numbers and {cards[str(card_num)]} copies")
            winners += cards[str(card_num)]
            print(f"Num winnings cards so far is {winners}")

        print(f"Total winning cards is {winners}")
        print(f"Total winning cards is {sum(cards.values())}")
