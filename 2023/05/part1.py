#!/usr/bin/env python3

class Map:

    def __init__(self):
        self.ranges = []

    def get(self, src):
        for dest_start, src_start, length in self.ranges:
            offset = src - src_start
            if offset >= 0 and offset < length:
                return dest_start + offset
        return src


if __name__ == "__main__":
    with open("input.txt") as infile:
        seeds = []
        maps: dict[str, Map] = {}
        curr_map_name = None

        for line in infile.readlines():
            line = line.strip()

            if line == "":
                continue

            elif line.startswith("seeds:"):
                seeds = [int(s) for s in line.split(":")[1].split()]

            elif line.endswith(" map:"):
                curr_map_name = line.split()[0]
                maps[curr_map_name] = Map()

            elif curr_map_name is None:
                raise ValueError("Found a mapping entry but no current map")

            else:
                maps[curr_map_name].ranges.append([int(v) for v in line.split()])

        closest_location = None

        for seed in seeds:
            soil = maps["seed-to-soil"].get(seed)
            fertilizer = maps["soil-to-fertilizer"].get(soil)
            water = maps["fertilizer-to-water"].get(fertilizer)
            light = maps["water-to-light"].get(water)
            temperature = maps["light-to-temperature"].get(light)
            humidity = maps["temperature-to-humidity"].get(temperature)
            location = maps["humidity-to-location"].get(humidity)
            if closest_location is None or location < closest_location:
                closest_location = location

        print(closest_location)