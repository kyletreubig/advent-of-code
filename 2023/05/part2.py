#!/usr/bin/env python3

class Map:

    def __init__(self, name):
        self.name = name
        self.ranges = []

    def get(self, in_ranges):
        print(f"Mapping {len(in_ranges)} ranges from {self.name}")
        mapped = []
        for in_start, in_length in in_ranges:
            print(f"  mapping {in_start, in_length} to...")
            while in_length > 0:
                for dest_start, src_start, length in self.ranges:
                    offset = in_start - src_start
                    if offset >= 0 and offset < length:
                        mapped_length = min(in_length, length - offset)
                        mapped.append((dest_start + offset, mapped_length))
                        print(f"    ...{mapped[-1]}")
                        in_length = in_length - mapped_length
                        in_start = in_start + mapped_length
                        break # out of for-loop
                else:
                    # no range found, unmapped
                    mapped.append((in_start, in_length))
                    print(f"    ...{mapped[-1]} (no mapping found)")
                    in_length = 0
        return mapped


if __name__ == "__main__":
    with open("input.txt") as infile:
        seed_ranges = []
        maps: dict[str, Map] = {}
        curr_map_name = None

        for line in infile.readlines():
            line = line.strip()

            if line == "":
                continue

            elif line.startswith("seeds:"):
                seeds = [int(s) for s in line.split(":")[1].split()]
                idx = 0
                while idx < len(seeds):
                    seed_start = seeds[idx]
                    seed_length = seeds[idx + 1]
                    idx += 2
                    seed_ranges.append((seed_start, seed_length))

            elif line.endswith(" map:"):
                curr_map_name = line.split()[0]
                maps[curr_map_name] = Map(curr_map_name)

            elif curr_map_name is None:
                raise ValueError("Found a mapping entry but no current map")

            else:
                maps[curr_map_name].ranges.append([int(v) for v in line.split()])

        soil_ranges = maps["seed-to-soil"].get(seed_ranges)
        fertilizer_ranges = maps["soil-to-fertilizer"].get(soil_ranges)
        water_ranges = maps["fertilizer-to-water"].get(fertilizer_ranges)
        light_ranges = maps["water-to-light"].get(water_ranges)
        temperature_ranges = maps["light-to-temperature"].get(light_ranges)
        humidity_ranges = maps["temperature-to-humidity"].get(temperature_ranges)
        location_ranges = maps["humidity-to-location"].get(humidity_ranges)
        print(sorted(location_ranges)[0][0])