#!/usr/bin/env python3

if __name__ == "__main__":
    with open("input.txt") as infile:
        races = []
        for line in infile.readlines():
            line = line.strip()
            if line.startswith("Time:"):
                for t in line.split(":")[1].split():
                    races.append([int(t), 0])
            elif line.startswith("Distance:"):
                for i, d in enumerate(line.split(":")[1].split()):
                    races[i][1] = int(d)

        margin = 1

        for race_time, best_distance in races:
            print(f"Race was {race_time}ms with best distance of {best_distance}mm")
            ways_to_win = 0
            for hold_time in range(1, race_time):
                print(f"  Hold for {hold_time}ms")
                speed = hold_time
                move_time = race_time - hold_time
                distance = speed * move_time
                print(f"    to travel {distance}mm")
                if distance > best_distance:
                    ways_to_win += 1
            print(f"There are {ways_to_win} ways to win this race")
            margin *= ways_to_win

        print(f"The margin of error is {margin}")

