#!/usr/bin/env python3
import math

if __name__ == "__main__":
    with open("input.txt") as infile:
        race_time = 0
        best_distance = 0
        for line in infile.readlines():
            line = line.strip()
            if line.startswith("Time:"):
                race_time = int(line.split(":")[1].strip().replace(" ", ""))
            elif line.startswith("Distance:"):
                best_distance = int(line.split(":")[1].strip().replace(" ", ""))

        # Given d=distance, h=holdtime, r=racetime, m=movetime
        # And speed = holdtime
        # Then d = speed * time = h * m = h * (r - h)
        # Rewritten as quadratic, 0 = -h^2 + rh - d
        # So a=-1, b=r, c=-d
        # Then h = (-r +/- sprt(r^2 - 4d))/-2

        a = -1
        b = race_time
        c = -best_distance

        x1 = (-b + math.sqrt(math.pow(b, 2) - (4 * a * c))) / (2 * a)
        x2 = (-b - math.sqrt(math.pow(b, 2) - (4 * a * c))) / (2 * a)
        print((x1, x2))

        min_hold = math.ceil(x1)
        max_hold = math.floor(x2)
        ways_to_win = max_hold - min_hold + 1 # (include both min and max)

        print(f"There are {ways_to_win} ways to win this race")

