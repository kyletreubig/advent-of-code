#!/usr/bin/env python3

RANKED_CARDS = ["2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K", "A"]
FIVE_OF_A_KIND = "5kind"
FOUR_OF_A_KIND = "4kind"
FULL_HOUSE = "fullh"
THREE_OF_A_KIND = "3kind"
TWO_PAIR = "2pair"
ONE_PAIR = "1pair"
HIGH_CARD = "highc"
RANKED_TYPES = [
    HIGH_CARD,
    ONE_PAIR,
    TWO_PAIR,
    THREE_OF_A_KIND,
    FULL_HOUSE,
    FOUR_OF_A_KIND,
    FIVE_OF_A_KIND,
]

def get_hand_type(cards):
    unique_cards = set(cards)
    counts = set([cards.count(c) for c in unique_cards])
    if counts == {5}:
        return FIVE_OF_A_KIND
    elif counts == {4, 1}:
        return FOUR_OF_A_KIND
    elif counts == {3, 2}:
        return FULL_HOUSE
    elif counts == {3, 1, 1}:
        return THREE_OF_A_KIND
    elif counts == {2, 1}:
        if len(unique_cards) == 3:
            return TWO_PAIR
        elif len(unique_cards) == 4:
            return ONE_PAIR
    elif counts == {1}:
        return HIGH_CARD
    raise ValueError(f"Invalid hand type: {cards}")

class Hand:

    def __init__(self, cards, bid):
        self.cards = [c for c in cards]
        self.bid = int(bid)
        self.type = get_hand_type(self.cards)
        self.rank = RANKED_TYPES.index(self.type)

    def __lt__(self, other: "Hand"):
        if self.rank == other.rank:
            for idx in range(5):
                if self.cards[idx] != other.cards[idx]:
                    return RANKED_CARDS.index(self.cards[idx]) < RANKED_CARDS.index(other.cards[idx])
            return False
        return self.rank < other.rank

    def __repr__(self):
        return f"{''.join(self.cards)} {self.type} ({self.rank}) {self.bid}"

if __name__ == "__main__":
    with open("input.txt") as infile:
        hands = []
        for line in infile.readlines():
            cards, bid = line.split()
            hands.append(Hand(cards, bid))

        hands = sorted(hands)
        print(hands)

        total_winnings = 0
        for idx, hand in enumerate(hands):
            rank = idx + 1
            hand_winnings = rank * hand.bid
            print(f"Winnings for {hand=} with rank {rank} is {hand_winnings}")
            total_winnings += hand_winnings

        print(f"Total winnings = {total_winnings}")
