#!/usr/bin/env python3

if __name__ ==  "__main__":
    with open("input.txt") as infile:
        total = 0
        for line in infile.readlines():
            values = [[int(v) for v in line.split()]]
            row = 0
            while not all([v == 0 for v in values[row]]):
                new_vals = [values[row][i] - values[row][i - 1] for i in range(1, len(values[row]))]
                values.append(new_vals)
                row += 1
            print(f"All rows are are {values}")

            values[row].append(0)
            row -= 1
            while row >= 0:
                values[row].append(
                    values[row][-1] + values[row + 1][-1]
                )
                row -= 1
            print(f"All rows are are now {values}")

            total += values[0][-1]

        print(f"Total is {total}")
